const path = require('path')

module.exports = {
  experiments: {
    outputModule: true,
  },
  entry: './src/index.js',
  // output: {
  // },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'itsafilter.mjs',
    clean: true,
    library: {
      type: 'module',
    },
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          },
        },
      },
      {
        test: /\.hbs$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'handlebars-loader',
      },
      {
        test: /\.css$/i,
        exclude: /(node_modules|bower_components)/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
}
