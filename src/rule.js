export default class Rule {
  constructor(text, col, match = true) {
    this.text = text
    this.col = col
    this.match = match
    this._regex = new RegExp(this.text, 'iu')
    this.deactivated = false
    this.id = Date.now() + Math.random()
  }

  exec(str) {
    if (this.deactivated) return false
    if (this._regex.exec(str)) {
      return this.match
    }
    return !this.match
  }

  toString() {
    if (this.match) {
      return `${this.col.name} ∋ "${this.text}"`
    } else {
      return `${this.col.name} ∌ "${this.text}"`
    }
  }

  toTemplate() {
    return {
      text: this.toString(),
      id: this.id,
      match: this.match,
      deactivated: this.deactivated,
    }
  }

  pack() {
    return {
      text: this.text,
      col: this.col,
      match: this.match,
      deactivated: this.deactivated,
      id: this.id,
    }
  }
}
