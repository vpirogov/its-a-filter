import RuleSet from './ruleSet.js'

// not using localStorage, key = ''
const rs = new RuleSet('')

describe('Add, move and delete rules', () => {
  test('Add the first rule', () => {
    rs.addRule({ id: 42 })
    expect(rs.rules.length).toBe(1)
    expect(rs.rules[0].length).toBe(1)
    expect(rs.rules[0][0].id).toBe(42)
  })

  test('Adding the second rule', () => {
    rs.addRule({ id: 142 })
    expect(rs.rules.length).toBe(2)
    expect(rs.rules[1].length).toBe(1)
    expect(rs.rules[1][0].id).toBe(142)
  })

  test('Adding the third rule', () => {
    rs.addRule({ id: 242 })
    expect(rs.rules.length).toBe(3)
    expect(rs.rules[2].length).toBe(1)
    expect(rs.rules[2][0].id).toBe(242)
  })

  test('Move the third rule to the first line', () => {
    rs.moveRule(242, 0)
    expect(rs.rules.length).toBe(2)
    expect(rs.rules[0].length).toBe(2)
    expect(rs.rules[0][1].id).toBe(242)
  })

  test('Move the first rule to the new line', () => {
    rs.moveRule(42, 2)
    expect(rs.rules.length).toBe(3)
    expect(rs.rules[0].length).toBe(1)
    expect(rs.rules[0][0].id).toBe(242)
    expect(rs.rules[2][0].id).toBe(42)
  })

  test('Delete the second rule', () => {
    rs.deleteRule(142)
    expect(rs.rules.length).toBe(2)
    expect(rs.rules[1][0].id).toBe(42)
  })
})

describe('Update rule', () => {
  test('Rule initialization', () => {
    rs.addRule({ id: 333, match: true, deactivated: false })
    expect(rs.rules[2][0].match).toBe(true)
    expect(rs.rules[2][0].deactivated).toBe(false)
  })

  test('Toggle Rule matching', () => {
    rs.toggleRuleMatching(333)
    expect(rs.rules[2][0].match).toBe(false)
  })

  test('Toggle Rule activity', () => {
    rs.toggleRuleActivity(333)
    expect(rs.rules[2][0].deactivated).toBe(true)
  })
})

describe('Update line', () => {
  test('Line initialization', () => {
    rs.addRule({ id: 500, deactivated: true })
    rs.addRule({ id: 600, deactivated: false })
    rs.moveRule(600, 3)
    rs.addRule({ id: 700, deactivated: true })
    rs.moveRule(700, 3)
    expect(rs.rules.length).toBe(4)
    expect(rs.rules[3].length).toBe(3)
  })

  test('The first toggle of the line activity', () => {
    rs.toggleLineActivity(3)
    rs.rules[3].forEach((rule) => {
      expect(rule.deactivated).toBe(false)
    })
  })

  test('The second toggle of the line activity', () => {
    rs.toggleLineActivity(3)
    rs.rules[3].forEach((rule) => {
      expect(rule.deactivated).toBe(true)
    })
  })
})
