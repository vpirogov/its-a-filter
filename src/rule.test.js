import Rule from './rule.js'

const r1 = new Rule('world', { id: 1, name: '*' }, true)
const r2 = new Rule('hello', { id: 2, name: 'address' }, false)

const phrases = ['hello world', 'hElLo wOrLd!', 'lo wo', 'hi there']

describe('Constructor works correctly', () => {
  test('"text" property is correct', () => {
    expect(r1.text).toBe('world')
    expect(r2.text).toBe('hello')
  })
  test('"match" property is correct', () => {
    expect(r1.match).toBe(true)
    expect(r2.match).toBe(false)
  })
  test('"deactivated" property is false by default', () => {
    expect(r1.deactivated).toBe(false)
    expect(r2.deactivated).toBe(false)
  })
  test('"id" property is correct', () => {
    expect(typeof r1.id).toBe('number')
    expect(typeof r2.id).toBe('number')
    expect(r1.id).not.toBe(r2.id)
  })
  test('"col" property is correct', () => {
    expect(r1.col).toHaveProperty('id', 1)
    expect(r2.col).toHaveProperty('id', 2)
    expect(r1.col).toHaveProperty('name', '*')
    expect(r2.col).toHaveProperty('name', 'address')
  })
})

describe('Methods works correctly', () => {
  test('toString()', () => {
    expect(r1.toString()).toBe('* ∋ "world"')
    r2.match = false
    expect(r2.toString()).toBe('address ∌ "hello"')
  })

  test('toTemplate()', () => {
    expect(r1.toTemplate()).toHaveProperty('text', '* ∋ "world"')
    expect(r1.toTemplate()).toHaveProperty('id')
    expect(typeof r1.toTemplate().id).toBe('number')
    expect(r1.toTemplate()).toHaveProperty('match', true)
    expect(r1.toTemplate()).toHaveProperty('deactivated', false)

    expect(r2.toTemplate()).toHaveProperty('text', 'address ∌ "hello"')
    expect(r2.toTemplate()).toHaveProperty('id')
    expect(typeof r2.toTemplate().id).toBe('number')
    expect(r2.toTemplate()).toHaveProperty('match', false)
    expect(r2.toTemplate()).toHaveProperty('deactivated', false)
  })

  test('pack()', () => {
    expect(r1.pack()).toHaveProperty('text', 'world')
    expect(r1.pack()).toHaveProperty('id')
    expect(typeof r1.pack().id).toBe('number')
    expect(r1.pack()).toHaveProperty('match', true)
    expect(r1.pack()).toHaveProperty('deactivated', false)
    expect(r1.pack()).toHaveProperty('col')
    expect(r1.pack().col).toHaveProperty('id', 1)
    expect(r1.pack().col).toHaveProperty('name', '*')

    expect(r2.pack()).toHaveProperty('text', 'hello')
    expect(r2.pack()).toHaveProperty('id')
    expect(typeof r2.pack().id).toBe('number')
    expect(r2.pack()).toHaveProperty('match', false)
    expect(r2.pack()).toHaveProperty('deactivated', false)
    expect(r2.pack()).toHaveProperty('col')
    expect(r2.pack().col).toHaveProperty('id', 2)
    expect(r2.pack().col).toHaveProperty('name', 'address')
  })

  test('exec()', () => {
    let arr = phrases.map((ph) => r1.exec(ph))
    expect(arr).toEqual(expect.arrayContaining([true, true, false, false]))
    arr = phrases.map((ph) => r1.exec(ph))
    expect(arr).toEqual(expect.arrayContaining([true, true, false, false]))
  })
})
