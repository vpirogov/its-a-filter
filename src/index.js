import _ from './static/style.css'

import RuleSet from './ruleSet.js'
import Rule from './rule.js'
import Page from './page.js'
import getStorageKey from './storageKey.js'

const WatcherTimeout = 500
const GuardTimeout = 10000
const Events = [
  'add-rule',
  'delete-rule',
  'move-rule',
  'toggle-rule-matching',
  'toggle-rule-activity',
  'toggle-line-activity',
]

export default class ItsAFilter extends HTMLElement {
  constructor() {
    super()

    this.tableSelector = null

    this.rs = null
    this.page = null

    this.watcher = null
    this.guard = null
  }

  connectedCallback() {
    this.tableSelector = this.getAttribute('table')
    const storageKey = this.getAttribute('key') || getStorageKey()

    // wait for table
    this.watcher = setInterval(() => {
      const table = document.querySelector(this.tableSelector)
      if (table && table.tagName === 'TABLE') {
        this.init(table, storageKey)
      }
    }, WatcherTimeout)

    this.guard = setTimeout(() => {
      throw new Error(
        `Its-A-Filter cannot find TABLE by selector "${this.tableSelector}"`
      )
    }, GuardTimeout)
  }

  init(table, storageKey) {
    clearInterval(this.watcher)
    clearTimeout(this.guard)

    this.rs = new RuleSet(storageKey)
    this.page = new Page(table, this)

    Events.forEach((eventName) => this.page.addEventListener(eventName, this))

    this._execute()
  }

  disconnectedCallback() {
    Events.forEach((eventName) =>
      this.page.removeEventListener(eventName, this)
    )
  }

  handleEvent(event) {
    event.stopPropagation()

    switch (event.type) {
      case 'add-rule':
        const { text, col } = event.detail.rule
        this.rs.addRule(new Rule(text, col, true))
        break
      case 'delete-rule':
        this.rs.deleteRule(event.detail.ruleId)
        break
      case 'move-rule':
        const { ruleId, lineId } = event.detail
        this.rs.moveRule(ruleId, lineId)
        break
      case 'toggle-rule-matching':
        this.rs.toggleRuleMatching(event.detail.ruleId)
        break
      case 'toggle-rule-activity':
        this.rs.toggleRuleActivity(event.detail.ruleId)
        break
      case 'toggle-line-activity':
        this.rs.toggleLineActivity(event.detail.lineId)
        break
    }

    this._execute()
    this.dispatchEvent(new CustomEvent('itsafilter-change'))
  }

  _execute() {
    this.page.writeToPage(this.rs.toTemplate())
    this.page.filterTable(this.rs.activeRules)
  }
}
