import filterTemplate from './templates/filter.hbs'

// import Handlebars from 'handlebars'
// import filterTemplate from './templates/filter.hbs.js'

// const filterTemplate = Handlebars.compile('./templates/filter.hbs')

export default class Page extends EventTarget {
  constructor(table, filter) {
    super()

    this._page_table = table

    this._page_target = filter

    this._page_target.innerHTML = 'qwe'

    this._page_cols = getColumnNames(this._page_table)
    if (!this._page_cols || !Array.isArray(this._page_cols)) {
      throw new Error(`Cannot get column names`)
    }

    this._page_target.addEventListener('click', this)
    this._page_target.addEventListener('drop', this)
  }

  handleEvent(event) {
    event.stopPropagation()
    const t = event.target
    switch (event.type) {
      case 'click':
        // add a rule
        if (t.matches('input#addbtn')) {
          this.dispatchEvent(
            new CustomEvent('add-rule', {
              detail: { rule: this._page_read() },
            })
          )
        }
        // change the rule
        if (t.matches('div.rule')) {
          const ruleId = +t.id
          const eventName = event.ctrlKey
            ? 'toggle-rule-matching'
            : 'toggle-rule-activity'

          this.dispatchEvent(
            new CustomEvent(eventName, {
              detail: { ruleId },
            })
          )
        }
        // delete the rule
        if (t.matches('span.delete')) {
          const ruleId = +t.closest('div.rule').id
          this.dispatchEvent(
            new CustomEvent('delete-rule', {
              detail: { ruleId },
            })
          )
        }
        // toggle line of rules
        if (t.matches('div.ruleline')) {
          const lineId = +t.dataset.lineid
          this.dispatchEvent(
            new CustomEvent('toggle-line-activity', {
              detail: { lineId },
            })
          )
        }
        break
      // drag'n'drop rule to another line
      case 'drop':
        event.preventDefault()
        const dropTarget = t.closest('div.ruleline')

        // extract ruleId from draggable and lineId from drop target
        let ruleId = +event.dataTransfer.getData('ruleId')
        let lineId = +dropTarget.closest('div.ruleline').dataset.lineid
        this.dispatchEvent(
          new CustomEvent('move-rule', {
            detail: { ruleId, lineId },
          })
        )
        break
    }
  }

  /* 
  && logic for lines
  || logic for rules into the line
  There is at least one rule per line.

  filter = [
    // line 0
    [rule,rule ...]
    // line 1
    [rule...]
    ...
    // line X
    [rule, rule ...]
  ]
  */

  filterTable(filter) {
    const rows = getRows(this._page_table)
    const mask = new Array(rows.length).fill(true)

    // loop over lines of rules
    filter.forEach((line) => {
      // loop over mask array (mask index == table rows index)
      byRows: for (let i = 0; i < mask.length; i++) {
        // do not process previously unmasked rows
        if (!mask[i]) continue

        // loop over rules of the line
        for (let rule of line) {
          if (rule.col.id > -1) {
            // any column rule - loop over cells of the row
            const text = rows[i].cells[rule.col.id].textContent
            if (rule.exec(text)) {
              continue byRows
            }
          } else {
            // specific column rule
            for (let cell of rows[i].cells) {
              const text = cell.textContent
              if (rule.exec(text)) {
                continue byRows
              }
            }
          }
        }

        // no matches for the line of rules => unmask the row
        mask[i] = false
      }
    })

    // apply mask to the table rows
    mask.forEach((m, idx) => {
      rows[idx].style.display = m ? '' : 'none'
    })
  }

  writeToPage(rules) {
    this._page_target.innerHTML = filterTemplate({
      cols: this._page_cols,
      rules,
    })
  }

  _page_read() {
    const colId = +this._page_target.querySelector('select').value
    const obj = {
      col: { id: colId, name: this._page_cols[colId] || '*' },
      text: this._page_target.querySelector('input[type=text]').value,
    }
    return obj
  }
}

function getColumnNames(table) {
  // from the first row of the table
  return Array.from(table.rows[0].cells).map((cell) => cell.innerHTML.trim())
}

function getRows(table) {
  // all rows except first
  const rows = Array.from(table.rows)
  rows.shift()
  return rows
}
