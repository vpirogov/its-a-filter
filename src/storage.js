import Rule from './rule.js'

// TODO refactor
export function loadRules(key) {
  if (key) {
    const packedString = localStorage.getItem(key) || ''
    if (!packedString) return []

    let packed = JSON.parse(fromBase64(packedString))
    packed = packed.map((line) => line.map((rule) => restoreRule(rule)))
    return packed
  }
  return []
}

export function saveRules(key, rules) {
  if (key) {
    const packed = []

    rules.forEach((line) => {
      const packedLine = []
      line.forEach((rule) => {
        packedLine.push(rule.pack())
      })
      packed.push(packedLine)
    })

    localStorage.setItem(key, toBase64(JSON.stringify(packed)))
  }
}

function restoreRule({ text, col, match, deactivated, id }) {
  const rule = new Rule(text, col, match)
  rule.deactivated = deactivated
  rule.id = id
  return rule
}

function toBase64(str) {
  return btoa(unescape(encodeURIComponent(str)))
}

function fromBase64(str) {
  return decodeURIComponent(escape(atob(str)))
}
