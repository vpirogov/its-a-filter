import Murmur from 'imurmurhash'

export default function getstorageKey() {
  const key = 'itsafilter_' + Murmur(window.location.toString()).result()
  return key
}
