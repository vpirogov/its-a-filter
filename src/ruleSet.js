import { loadRules, saveRules } from './storage.js'

export default class RuleSet {
  constructor(localStorageKey) {
    this.key = localStorageKey
    this.rules = loadRules(this.key)
  }

  addRule(rule) {
    this.rules.push([rule])
    saveRules(this.key, this.rules)
  }

  deleteRule(id) {
    const { i, j } = this._findById(id)
    if (this.rules[i].length === 1) {
      this.rules.splice(i, 1)
    } else {
      this.rules[i].splice(j, 1)
    }
    saveRules(this.key, this.rules)
  }

  moveRule(ruleId, lineId) {
    // find rule and actual line id
    const { i, rule } = this._findById(ruleId)
    if (i === lineId) return

    // adjust lineId on previous line removal
    if (i < lineId && this.rules[i].length === 1) lineId--

    // move rule to another line
    this.deleteRule(ruleId)
    if (lineId < this.rules.length) {
      this.rules[lineId].push(rule)
    } else {
      this.rules.push([rule])
    }
    saveRules(this.key, this.rules)
  }

  toggleRuleMatching(id) {
    const { rule } = this._findById(id)
    rule.match = !rule.match
    saveRules(this.key, this.rules)
  }

  toggleRuleActivity(id) {
    const { rule } = this._findById(id)
    rule.deactivated = !rule.deactivated
    saveRules(this.key, this.rules)
  }

  toggleLineActivity(lineid) {
    if (lineid >= this.rules.length) return
    const line = this.rules[lineid]
    const deactived = !line[0].deactivated
    line.forEach((rule) => (rule.deactivated = deactived))
    saveRules(this.key, this.rules)
  }

  _findById(id) {
    for (let i = 0; i < this.rules.length; i++) {
      const line = this.rules[i]
      for (let j = 0; j < line.length; j++) {
        if (line[j].id === id) {
          return { i, j, rule: line[j] }
        }
      }
    }
  }

  toTemplate() {
    return this.rules.map((line) => line.map((rule) => rule.toTemplate()))
  }

  get activeRules() {
    const ar = this.rules.map((line) =>
      line.filter((rule) => !rule.deactivated)
    )
    return ar.filter((line) => line.length)
  }
}
