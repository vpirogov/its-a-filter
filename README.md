# its-a-filter

Filter widget for HTML tables.

## Usage:

1. Install
```
    npm install -D @vlsp/its-a-filter
```
2. Register custom element
```
    import ItsAFilter from '@vlsp/its-a-filter'

    customElements.define('its-a-filter', ItsAFilter)
```

3. Add element to HTML
```
    <its-a-filter table="table#mydata"></its-a-filter>
```

Where table attribute value should be equal to target table CSS selector.
